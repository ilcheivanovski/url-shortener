﻿using System;
using System.ComponentModel.DataAnnotations;

namespace UrlShortener.Models
{
    public class ShortUrl
    {
        [Key]
        public String Key { get; set; }

        [Required]
        public String Url { get; set; }

        public DateTime DateCreated { get; set; }
    }
}
