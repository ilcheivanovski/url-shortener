﻿using System.Data.Entity;
using UrlShortener.Models;

namespace UrlShortener.Data
{
    internal class DatabaseContext: DbContext
    {
        public DatabaseContext() : base("ShortUrl")
        {
            Database.SetInitializer<DatabaseContext>(new CreateDatabaseIfNotExists<DatabaseContext>());
        }

        public DbSet<ShortUrl> ShortUrls { get; set; }
    }
}
