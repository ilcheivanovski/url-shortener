﻿using UrlShortener.Logic;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace UrlShortener.Web.Controllers
{
    public class UrlController : ApiController
    {
        /// <summary>
        /// Get real URL by providing key as url parameter
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("{key}")]
        public HttpResponseMessage Get(string key)
        {
            using (UrlManager urlManager = new UrlManager())
            {
                // The default action when this status is received is to follow the Location
                // header associated with the response. 
                var response = Request.CreateResponse(HttpStatusCode.Moved);
                String urlString = urlManager.GetUrl(key);
                if (!String.IsNullOrWhiteSpace(urlString))
                {
                    response.Headers.Location = new Uri(urlString);
                    return response;
                }
            }
            return null;
        }

        /// <summary>
        /// Redirect to real URL by enter of short url
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("short")]
        public HttpResponseMessage Post([FromBody] String url)
        {
            using (UrlManager urlManager = new UrlManager())
            {
                var scheme = Request.RequestUri.Scheme;
                var host = Request.RequestUri.Host;
                return Request.CreateResponse(HttpStatusCode.OK, urlManager.AddShortUrl(url, scheme, host));
            }
        }
    }
}
