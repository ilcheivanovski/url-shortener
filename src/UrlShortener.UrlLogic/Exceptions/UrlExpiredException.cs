﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UrlShortener.Logic.Exceptions
{
    public class UrlExpiredException : Exception
    {
        public UrlExpiredException(Exception innerException) : base(Resources.ExceptionMessages.UrlExpiredException, innerException)
        {

        }
    }
}
