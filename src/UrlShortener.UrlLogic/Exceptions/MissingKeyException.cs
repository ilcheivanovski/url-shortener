﻿using System;

namespace UrlShortener.Logic.Exceptions
{
    public class MissingKeyException : Exception
    {
        public MissingKeyException(Exception innerException) : base(Resources.ExceptionMessages.MissingKeyException, innerException)
        {

        }
    }
}
