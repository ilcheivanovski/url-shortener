﻿using System.Configuration;

namespace UrlShortener.Logic
{
    /// <summary>
    /// Service settings that are controlling behaviour of URL shortening web service.
    /// </summary>
    public static class ConfigManager
    {
        public static bool CheckUrlAvailability
        {
            get
            {
                bool check;
                if (!bool.TryParse(ConfigurationManager.AppSettings["CheckUrlAvailability"], out check))
                {
                    check = false;
                }
                return check;
            }
        }

        public static int KeyLength
        {
            get
            {
                int keyLength;
                if (!int.TryParse(ConfigurationManager.AppSettings["KeyLength"], out keyLength))
                {
                    keyLength = 6;
                }
                return keyLength;
            }
        }

        public static int CacheTimeout
        {
            get
            {
                int cacheTimeout;
                if (!int.TryParse(ConfigurationManager.AppSettings["CacheTimeout"], out cacheTimeout))
                {
                    cacheTimeout = 5;
                }
                return cacheTimeout;
            }
        }

        public static int ExpireTime
        {
            get
            {
                int expireTime;
                if (!int.TryParse(ConfigurationManager.AppSettings["ExpireTime"], out expireTime))
                {
                    expireTime = 5;
                }
                return expireTime;
            }
        }
    }
}
