﻿using System;
using System.Web;
using System.Web.Caching;

namespace UrlShortener.Logic
{
    public static class CacheManager
    {
        /// <summary>
        /// Get cache by key (for the current application domain)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="cacheKey"></param>
        /// <returns></returns>
        public static T GetCached<T>(string cacheKey) where T : class
        {
            HttpContext httpContext = HttpContext.Current;
            if (httpContext != null)
            {
                return HttpContext.Current.Cache[cacheKey] as T;
            }   
            return null;
        }

        /// <summary>
        /// Add value to cache (for the current application domain)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="cacheKey"></param>
        /// <param name="value"></param>
        /// <param name="timeout"></param>
        /// <returns></returns>
        public static bool TryAddToCache<T>(string cacheKey, T value, int timeout = 0)
        {
            HttpContext httpContext = HttpContext.Current;
            if (httpContext != null)
            {
                httpContext.Cache.Insert(cacheKey, value, null, Cache.NoAbsoluteExpiration, TimeSpan.FromMinutes(ConfigManager.CacheTimeout));
            }
            return false;
        }

    }
}
