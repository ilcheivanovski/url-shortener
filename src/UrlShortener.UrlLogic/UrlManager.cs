﻿using System;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using UrlShortener.Models;

namespace UrlShortener.Logic
{
    /// <summary>
    /// URL manager module for managing URL's
    /// </summary>
    public class UrlManager : IDisposable
    {
        private UrlShortener.Data.IRepository<ShortUrl> dataRepository;
        private bool disposing = false;

        public UrlManager()
        {
            dataRepository = new Data.UrlRepository();
        }

        /// <summary>
        /// Save url key in database and return full short url(/w domain name and protocol)
        /// </summary>
        /// <param name="Url"></param>
        /// <param name="scheme"></param>
        /// <param name="host"></param>
        /// <returns></returns>
        public String AddShortUrl(string Url, string scheme, string host)
        {
            if (!string.IsNullOrWhiteSpace(Url))
            {
                Url = Url.Trim().ToLower();
            }

            // get cache ffor the current application domain
            String cached = null;
            cached = CacheManager.GetCached<String>(Url);
            if (!String.IsNullOrWhiteSpace(cached))
            {
                return cached;
            }

            try
            {
                var url = new Uri(Url);
            }
            catch (Exception ex)
            {
                throw new Exceptions.InvalidUrlException(ex);
            }

            if ((ConfigManager.CheckUrlAvailability && this.HttpGetStatusCode(Url).Result == HttpStatusCode.OK) || !ConfigManager.CheckUrlAvailability)
            {

                String newKey = null;
                while (string.IsNullOrEmpty(newKey))
                {
                    if (!dataRepository.ExistsUrl(Url))
                    {
                        /// creat unique string with key length from web.config
                        newKey = Guid.NewGuid().ToString("N").Substring(0, ConfigManager.KeyLength).ToLower();
                        dataRepository.Add(new ShortUrl() { Key = newKey, Url = Url, DateCreated = DateTime.UtcNow });
                    }
                    else
                    {
                        var shortUrl = dataRepository.FindUrl(Url);
                        if (shortUrl != null)
                        {
                            newKey = shortUrl.Key;
                        }
                    }
                }
                var url = $"{scheme}://{host}/{newKey}";

                /// add the newKey to cache for the current domain
                CacheManager.TryAddToCache<String>(Url, url);

                return url;
            }
            else
            {
                throw new Exceptions.UrlUnreachableException(null);
            }
        }

        /// <summary>
        /// Get real url by short url key
        /// </summary>
        /// <param name="ShortUrlKey"></param>
        /// <returns></returns>
        public String GetUrl(String ShortUrlKey)
        {
            var url = dataRepository.FindKey(ShortUrlKey);
            if (url != null)
            {
                var isExpired = this.IsExpired(url.DateCreated);
                return
                     isExpired
                     ? throw new Exceptions.UrlExpiredException(null)
                     : url.Url;
            }
            else
            {
                throw new Exceptions.MissingKeyException(null);
            }
        }

        /// <summary>
        /// Check if short url is expired
        /// </summary>
        /// <param name="created"></param>
        /// <returns></returns>
        private bool IsExpired(DateTime created)
        {
            var months = ConfigManager.ExpireTime;
            return DateTime.UtcNow > created.AddMonths(months);
        }

        /// <summary>
        /// Check if the real url is valid
        /// </summary>
        /// <param name="Url"></param>
        /// <returns></returns>
        public async Task<HttpStatusCode> HttpGetStatusCode(string Url)
        {
            try
            {
                var httpclient = new HttpClient();
                var response = await httpclient.GetAsync(Url, HttpCompletionOption.ResponseHeadersRead).ConfigureAwait(continueOnCapturedContext: false);
                return response.StatusCode;
            }
            catch (Exception ex)
            {
                return HttpStatusCode.NotFound;
            }
        }

        public void Dispose()
        {
            if (!disposing)
            {
                disposing = true;
                if (this.dataRepository != null)
                {
                    this.dataRepository.Dispose();
                }
            }
        }
    }
}
